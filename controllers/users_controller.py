# Copyright (c) 2024 Intracom Telecom
# 
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
# 
#     http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# 
# SPDX-License-Identifier: Apache-2.0
# 
# Contributors:
#     [name] - [contribution]

from pathlib import Path

import pymongo
from flask import Flask, request

app = Flask(__name__)
local_path = Path(__file__).parent

storage_url = 'mongodb://mongoapis'
port = 27017
mydb_mongo = 'location_api_users'


@app.route('/queries/users', methods=['GET'])
def queries_users():
    zone_id = request.args.getlist('zoneId')
    access_point_id = request.args.getlist('accessPointId')
    address = request.args.getlist('address')

    my_client = pymongo.MongoClient(storage_url)
    db = my_client[mydb_mongo]  # select database
    col = db['UserInfo']  # select collection
    query = {}
    if address:
        query['address'] = {"$in": address}
    if zone_id:
        query['zoneId'] = {"$in": zone_id}
    if access_point_id:
        query['accessPointId'] = {"$in": access_point_id}
    users_info = list(col.find(query, {'_id': 0}))
    return {"userList": {"user": users_info, "resourceURL": "https://example.com/exampleAPI/location/v2/queries/users"}}
