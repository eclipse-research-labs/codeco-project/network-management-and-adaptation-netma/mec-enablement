# Copyright (c) 2024 Intracom Telecom
# 
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
# 
#     http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# 
# SPDX-License-Identifier: Apache-2.0
# 
# Contributors:
#     [name] - [contribution]

from pathlib import Path

import pymongo
from flask import Flask, request

app = Flask(__name__)
local_path = Path(__file__).parent

storage_url = 'mongodb://mongoapis'
port = 27017
mydb_mongo = 'location_api_users'


@app.route('/queries/zones', methods=['GET'])
def queries_zones():
    zones = request.args.getlist('zoneId')
    my_client = pymongo.MongoClient(storage_url)
    db = my_client[mydb_mongo]  # select database
    col = db['ZoneInfo']  # select collection
    params = {'zoneId': {"$in": zones}}
    zones_info = [_ for _ in col.find(params, {'_id': 0})]
    return {"zoneList": {"zone": zones_info, "resourceURL": "https://example.com/exampleAPI/location/v2/queries/zones"}}


@app.route('/queries/zones/<string:zoneId>', methods=['GET'])
def queries_zone(zoneId):
    my_client = pymongo.MongoClient(storage_url)
    db = my_client[mydb_mongo]  # select database
    col = db['ZoneInfo']  # select collection
    zones_info = list(col.find({'zoneId': zoneId}, {'_id': 0}))
    return {"zoneInfo": zones_info}


@app.route('/queries/zones/<string:zoneId>/accessPoints', methods=['GET'])
def queries_zone_access_points(zoneId):
    access_point = request.args.getlist('accessPointId')
    my_client = pymongo.MongoClient(storage_url)
    db = my_client[mydb_mongo]  # select database
    col = db['AccessPointInfo']
    query = {'zoneId': zoneId, 'accessPointId': {"$in": access_point}}
    access_point_info = list(col.find(query, {'_id': 0}))
    return {"accessPointList": {"accessPoint": access_point_info, "resourceURL": "https://example.com/exampleAPI/location/v2/queries/users"}, "zoneId": zoneId}
