# Copyright (c) 2024 Intracom Telecom
# 
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
# 
#     http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# 
# SPDX-License-Identifier: Apache-2.0
# 
# Contributors:
#     [name] - [contribution]

from pathlib import Path

import pymongo
from flask import Flask, request, jsonify
from pymongo import MongoClient

app = Flask(__name__)
local_path = Path(__file__).parent

storage_url = 'mongodb://mongoapis'
port = 27017
mydb_mongo = 'location_api_users'


def create_post(collection_name, req):
    myclient = MongoClient(storage_url, port)
    db = myclient[mydb_mongo]
    collection = db[collection_name]
    collection.insert_one(req)
    req['_id'] = str(req.get('_id', ''))
    req.pop('_id', None)
    return jsonify(req)


@app.route('/subscriptions/distance', methods=['POST'])
def create_distance_subscription():
    req = request.get_json()
    return create_post("SubscriptionsDistance", req)


@app.route('/subscriptions/distance', methods=['GET'])
def retrieve_distance_subscriptions():
    collection_name = "SubscriptionsDistance"
    myclient = MongoClient(storage_url, port)
    db = myclient[mydb_mongo]
    collection = db[collection_name]
    subscriptions = list(collection.find())
    result = {
        "notificationSubscriptionList": {
            "resourceURL": {
                "href": "test/testing"
            },
            "subscription": [
            ]
        }
    }
    for subscription in subscriptions:
        result["notificationSubscriptionList"]['subscription'].append(
            {
                "href": subscription['userDistanceSubscription']["_links"]['self']['href'],
                "subscriptionType": subscription['userDistanceSubscription']['subscriptionType']
            })

    return result


@app.route('/subscriptions/area', methods=['POST'])
def create_area_subscription():
    req = request.get_json()
    return create_post('SubscriptionsArea', req)


@app.route('/subscriptions/area', methods=['GET'])
def retrieve_area_subscriptions():
    collection_name = "SubscriptionsArea"
    myclient = MongoClient(storage_url, port)
    db = myclient[mydb_mongo]
    collection = db[collection_name]
    subscriptions = list(collection.find())
    result = {
        "notificationSubscriptionList": {
            "resourceURL": {
                "href": "test/testing"
            },
            "subscription": [
            ]
        }
    }
    for subscription in subscriptions:
        result["notificationSubscriptionList"]['subscription'].append(
            {
                "href": subscription['userAreaSubscription']["_links"]['self']['href'],
                "subscriptionType": subscription['userAreaSubscription']['subscriptionType']
            })

    return result


@app.route('/subscriptions/zones', methods=['POST'])
def create_zones_subscription():
    req = request.get_json()
    return create_post('SubscriptionsZones', req)


@app.route('/subscriptions/zones', methods=['GET'])
def retrieve_zones_subscriptions():
    collection_name = "SubscriptionsZones"
    myclient = MongoClient(storage_url, port)
    db = myclient[mydb_mongo]
    collection = db[collection_name]
    subscriptions = list(collection.find())
    result = {
        "notificationSubscriptionList": {
            "resourceURL": {
                "href": "test/testing"
            },
            "subscription": [
            ]
        }
    }
    for subscription in subscriptions:
        result["notificationSubscriptionList"]['subscription'].append(
            {
                "href": subscription['zoneLocationEventSubscription']["_links"]['self']['href'],
                "subscriptionType": subscription['zoneLocationEventSubscription']['subscriptionType']
            })

    return result


@app.route('/subscriptions/users', methods=['POST'])
def create_users_subscription():
    req = request.get_json()
    return create_post('SubscriptionsUsers', req)


@app.route('/subscriptions/users', methods=['GET'])
def retrieve_users_subscriptions():
    collection_name = "SubscriptionsUsers"
    myclient = MongoClient(storage_url, port)
    db = myclient[mydb_mongo]
    collection = db[collection_name]
    subscriptions = list(collection.find())
    print(subscriptions)
    result = {
        "notificationSubscriptionList": {
            "resourceURL": {
                "href": "test/testing"
            },
            "subscription": [
            ]
        }
    }
    for subscription in subscriptions:
        result["notificationSubscriptionList"]['subscription'].append(
            {
                "href": subscription['userLocationPeriodicSubscription']["_links"]['self']['href'],
                "subscriptionType": subscription['userLocationPeriodicSubscription']['subscriptionType']
            })

    return result
