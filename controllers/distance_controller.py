# Copyright (c) 2024 Intracom Telecom
# 
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
# 
#     http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# 
# SPDX-License-Identifier: Apache-2.0
# 
# Contributors:
#     [name] - [contribution]

import time
from math import radians, cos, sin, asin, sqrt
from pathlib import Path

import pymongo
from flask import Flask, request

app = Flask(__name__)
local_path = Path(__file__).parent
storage_url = 'mongodb://mongoapis'
mydb_mongo = 'location_api_users'


def compute_distance(lat1, lat2, lon1, lon2):
    """
    A function to compute the distance between two points on earth
    given their coordinates, based on the Haversine Formula.
    """

    # Radius of earth in kilometers.
    R = 6371

    lat1, lat2, lon1, lon2 = [radians(el) for el in [lat1, lat2, lon1, lon2]]

    # Haversine formula
    dlon = lon2 - lon1
    dlat = lat2 - lat1
    a = sin(dlat / 2) ** 2 + cos(lat1) * cos(lat2) * sin(dlon / 2) ** 2

    c = 2 * asin(sqrt(a))

    return c * R


@app.route('/queries/distance', methods=['GET'])
def query_distance():
    addresses = request.args.getlist('address')

    my_client = pymongo.MongoClient(storage_url)
    db = my_client[mydb_mongo]  # select database
    col = db['UserInfo']  # select collection
    params = {'address': {"$in": addresses}}
    users_info = [_ for _ in col.find(params)]

    (lat1, lon1), *u2 = [(user['location']['latitude'], user['location']['longitude']) for user in users_info]

    if len(u2) == 1:
        lat2, lon2 = u2[0]
    else:
        lat2, lon2 = tuple(request.args.get('location').split(','))
        lat2 = float(lat2)
        lon2 = float(lon2)

    return {
        "terminalDistance": {
            "accuracy": 1,
            "distance": compute_distance(lat1, lat2, lon1, lon2) * 1000,
            "timestamp": {
                "nanoSeconds": time.time_ns(),
                "seconds": time.time()
            }
        }
    }
