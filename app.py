# Copyright (c) 2024 Intracom Telecom
# 
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
# 
#     http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# 
# SPDX-License-Identifier: Apache-2.0
# 
# Contributors:
#     [name] - [contribution]

from flask import Flask

from controllers.distance_controller import query_distance
from controllers.subscriptions_controller import create_distance_subscription, retrieve_distance_subscriptions, \
    create_area_subscription, retrieve_area_subscriptions, create_zones_subscription, retrieve_zones_subscriptions, \
    create_users_subscription, retrieve_users_subscriptions
from controllers.users_controller import queries_users
from controllers.zones_controller import queries_zones, queries_zone, queries_zone_access_points
from resources.mock_data_creation import create_mock_data

app = Flask(__name__)
create_mock_data()


@app.route('/')
def greeting():  # put application's code here
    return 'Welcome to the MEC Location API'


@app.route('/queries/distance', methods=['GET'])
def compute_user_distance():
    return query_distance()


@app.route('/queries/users', methods=['GET'])
def get_users_information():
    return queries_users()


@app.route('/queries/zones', methods=['GET'])
def get_zones_information():
    return queries_zones()


@app.route('/queries/zones/<string:zoneId>', methods=['GET'])
def get_zone_information(zoneId):
    return queries_zone(zoneId)


@app.route('/queries/zones/<string:zoneId>/accessPoints', methods=['GET'])
def get_zone_access_points_information(zoneId):
    return queries_zone_access_points(zoneId)


@app.route('/subscriptions/distance', methods=['POST'])
def post_distance_subscription():
    return create_distance_subscription()


@app.route('/subscriptions/distance', methods=['GET'])
def get_distance_subscription():
    return retrieve_distance_subscriptions()


@app.route('/subscriptions/area', methods=['POST'])
def post_area_subscription():
    return create_area_subscription()


@app.route('/subscriptions/area', methods=['GET'])
def get_area_subscription():
    return retrieve_area_subscriptions()


@app.route('/subscriptions/zones', methods=['POST'])
def post_zones_subscription():
    return create_zones_subscription()


@app.route('/subscriptions/zones', methods=['GET'])
def get_zones_subscription():
    return retrieve_zones_subscriptions()


@app.route('/subscriptions/users', methods=['POST'])
def post_users_subscription():
    return create_users_subscription()


@app.route('/subscriptions/users', methods=['GET'])
def get_users_subscription():
    return retrieve_users_subscriptions()


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5000)
