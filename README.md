<!--
  ~ Copyright (c) 2024 Intracom Telecom
  ~ 
  ~ Licensed under the Apache License, Version 2.0 (the "License");
  ~ you may not use this file except in compliance with the License.
  ~ You may obtain a copy of the License at
  ~ 
  ~     http://www.apache.org/licenses/LICENSE-2.0
  ~ 
  ~ Unless required by applicable law or agreed to in writing, software
  ~ distributed under the License is distributed on an "AS IS" BASIS,
  ~ WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  ~ See the License for the specific language governing permissions and
  ~ limitations under the License.
  ~ 
  ~ SPDX-License-Identifier: Apache-2.0
  ~ 
  ~ Contributors:
  ~     [name] - [contribution]
-->

**Prerequisites**

To use this code you should have the following:
- Python 3.9 Installed
- MongoDB Installed

**Description**

This repository contains the code of some endpoints of the MEC Location API. Each time the code is run, several collections, relevant to the MEC API, are populated with random values. At this point, the values aren't correlated.

**Endpoints**

The working endpoints are the following:

GET
- /queries/distance
- /queries/users
- /queries/zones
- /queries/zones/<<string:zoneId>>
- /queries/zones/<<string:zoneId>>/accessPoints
- /subscriptions/distance

POST
- /subscriptions/area
- /subscriptions/distance
- /subscriptions/users
- /subscriptions/zones

You can find instructions on how to call the endpoints at:

https://forge.etsi.org/swagger/ui/?url=https://forge.etsi.org/rep/mec/gs013-location-api/raw/master/LocationAPI.yaml#/

**Requirements**
- Jinja2==3.1.2
- MarkupSafe==2.1.3
- blinker==1.6.2
- click==8.1.7
- colorama==0.4.6
- dnspython==2.4.2
- flask==2.3.3
- importlib-metadata==6.8.0
- itsdangerous==2.1.2
- pip==22.3.1
- pymongo==4.5.0
- setuptools==65.5.1
- werkzeug==2.3.7
- wheel==0.38.4
- zipp==3.17.0

Dependencies can be installed using the following command:

`pip install -r requirements.txt`